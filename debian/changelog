pdd (1.7-2) unstable; urgency=medium

   * Fix source build issue after initial success (Closes: #1047151)
   * d/control:
    - Bump Standards-Version to 4.6.2

 -- SZ Lin (林上智) <szlin@debian.org>  Sat, 17 Feb 2024 00:13:56 +0800

pdd (1.7-1) unstable; urgency=medium

  * Import new upstream release
  * Fix d/watch issue
  * Tweak the patches to adopt the new release
  * Install auto-completion files
  * d/control:
    - Bump Standards-Version to 4.6.1.0
  * d/copyright:
    - Update copyright year

 -- SZ Lin (林上智) <szlin@debian.org>  Tue, 20 Dec 2022 16:01:25 +0800

pdd (1.5-1) unstable; urgency=medium

  * Import new upstream release
  * d/control:
    - Bump Standards-Version to 4.5.0
    - Bump debhelper-compat to 13

 -- SZ Lin (林上智) <szlin@debian.org>  Mon, 26 Oct 2020 16:29:27 +0800

pdd (1.4-2) unstable; urgency=low

  * d/control:
    - Bump Standards-Version to 4.4.1
    - Rules-Requires-Root: no
    - Use debhelper-compat (= 12) in build-dependency to replace d/compat
  * d/tests:
    - Add test case

 -- SZ Lin (林上智) <szlin@debian.org>  Fri, 29 Nov 2019 14:26:25 +0800

pdd (1.4-1) unstable; urgency=medium

  * Import new upstream release
  * Add upstream metadata
  * d/compat:
    - Bump version to 12
  * d/control:
    - Bump Standards-Version to 4.3
    - Bump debhelper to 12
    - Remove X-Python3-Version field
  * d/install:
    - Install real program instead of "entry script"
  * d/watch:
    - Fix malformed filenamemangle

 -- SZ Lin (林上智) <szlin@debian.org>  Thu, 04 Apr 2019 23:32:26 +0800

pdd (1.3.1-1) unstable; urgency=medium

  * Import new upstream release
  * d/control: bump Standards-Version to 4.2.0.1

 -- SZ Lin (林上智) <szlin@debian.org>  Mon, 13 Aug 2018 16:19:44 +0800

pdd (1.3-1) unstable; urgency=medium

  * Import new upstream release
  * d/rules: use pybuild as buildsystem
  * d/control: bump Standards-Version to 4.1.5
  * d/install: remove unnecessary file

 -- SZ Lin (林上智) <szlin@debian.org>  Thu, 19 Jul 2018 11:40:25 +0800

pdd (1.2-1) unstable; urgency=medium

  * Import new upstream release
  * Converting git-dpm to gbp pq
  * Refresh patches after git-dpm to gbp pq conversion
  * Update the path of manpage
  * d/control: Add build dependencies: python3-logilab-common, python3-pytest
               and python3-dateutil

 -- SZ Lin (林上智) <szlin@debian.org>  Mon, 21 May 2018 11:07:11 +0800

pdd (1.1-2) unstable; urgency=medium

  * d/control: bump Standards-Version to 4.1.4
  *            bump debhelper version to 11
  *            add missing build dependency: dh-python (Closes: #896734)
  * d/compat: bump compat version to 11

 -- SZ Lin (林上智) <szlin@debian.org>  Tue, 24 Apr 2018 15:29:10 +0800

pdd (1.1-1) unstable; urgency=medium

  * Import new upstream release
  * Bump Standards-Version to 4.1.0
  * Use "https" in d/copyright

 -- SZ Lin (林上智) <szlin@debian.org>  Tue, 26 Sep 2017 19:15:29 +0800

pdd (1.0-1) unstable; urgency=medium

  * Initial release (Closes: #869850)

 -- SZ Lin (林上智) <szlin@debian.org>  Fri, 28 Jul 2017 10:23:54 +0800
